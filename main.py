import libnacl, libnacl.utils

class encryption_1606:
    their_pk = None
    our_pk, our_sk = None, None
    
    def __init__(self):
        pass
        
    def set_our_pk(self, pk):
        self.check_key(pk)
        self.our_pk = pk
        
    def set_our_sk(self, sk):
        self.check_key(sk)
        self.our_sk = sk
        
    def set_their_pk(self, pk):
        self.check_key(pk)
        self.their_pk = pk
        
    def check_key(self, key):
        if key is None:
            raise TypeError('The key should not be empty.')
        elif type(key) is not bytes:
            raise TypeError(f'The key supplied should be bytes, not {type(key)}')
        elif len(key) != 32:
            raise ValueError(f'32 length expected for the supplied key, not {len(key)}')
        
    def check_ptxt(self, ptxt):
        if type(ptxt) is str and len(ptxt) == 0:
            raise ValueError('Expected non zero length of plaintext.')
        elif type(ptxt) is not str and type(ptxt) is not str:
            raise TypeError(f'Expected str/bytes as plaintext, not {type(ptxt)}')
        
    def process_ptxt(self, ptxt):
        # check plaintext if valid,
        # and change to bytes if is str
        self.check_ptxt(ptxt)
        
        if type(ptxt) is bytes:
            return ptxt
        elif type(ptxt) is str and len(ptxt) > 0:
            return str.encode(ptxt)
        elif type(ptxt) is not bytes and type(ptxt) is not str:
            raise TypeError(f'Expected bytes or str as ptxt, not {str(type(ptxt))}')
        else:
            raise Exception('check_ptxt does not raise, but the ptxt is still invalid')
    
    def check_ctxt(self, ctxt):
        if ctxt is None:
            raise TypeError('The ciphertext should not be empty.')
        elif type(ctxt) is not bytes:
            raise TypeError(f'The ciphertext supplied should be bytes, not {type(ctxt)}')
        elif len(ctxt) < 25:
            raise ValueError(f'Malform ciphertext: > 25 length expected, not {len(ctxt)}')
    
    def process_ctxt(self, ctxt):
        # check if ciphertext is valid,
        # and separate into nonce_part and box_part
        self.check_ctxt(ctxt)
        
        nonce_part = ctxt[0:24]
        box_part = ctxt[24:]
        
        return nonce_part, box_part
                
    def anon_encrypt(self, ptxt):
        self.check_key(self.their_pk)
        ptxt = self.process_ptxt(ptxt)
        
        return libnacl.crypto_box_seal(ptxt, self.their_pk)
    
    def gen_nonce(self):        
        nonce = libnacl.utils.rand_nonce()
        
        if type(nonce) is bytes and len(nonce) == 24:
            return nonce
        else:
            raise Exception('Cannot generate a valid nonce.')
    
    def sign_and_encrypt(self, ptxt):
        self.check_key(self.their_pk)
        self.check_key(self.our_sk)
        
        ptxt = self.process_ptxt(ptxt)
        
        nonce = self.gen_nonce()
        
        # box = ciphertext
        box = libnacl.crypto_box(ptxt, nonce, self.their_pk, self.our_sk)
        
        # msg = nonce(bytes, len=24) + box(bytes, len>0)
        msg = nonce
        msg += box
        
        return msg
    
    def decrypt_and_verify_sig(self, ctxt):
        self.check_key(self.their_pk)
        self.check_key(self.our_sk)
        
        nonce, box = self.process_ctxt(ctxt)
        
        try:
            c_ptxt = libnacl.crypto_box_open(box, nonce, self.their_pk, self.our_sk)
        except libnacl.CryptError as err:
            raise ValueError(err)
        
        return c_ptxt
    
    def anon_decrypt(self, ctxt):
        self.check_key(self.our_pk)
        self.check_key(self.our_sk)
        self.check_ctxt(ctxt)
        
        c_ptxt = libnacl.crypto_box_seal_open(ctxt, self.our_pk, self.our_sk)
        
        return c_ptxt

def xor_bytes(bytes_in, key):
    if type(bytes_in) != bytes or type(key) != bytes:
        raise TypeError('Both key and plaintext should be bytes.')
    if len(bytes_in) > len(key):
        raise ValueError('Key should be longer than plaintext.')
    return bytes([a ^ b for a, b in zip(bytes_in, key)])